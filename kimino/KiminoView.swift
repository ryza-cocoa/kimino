//
//  KiminoWindow.swift
//  kimino
//
//  Created by Ryza on 8/11/2019.
//  Copyright © 2019 Ryza. All rights reserved.
//

import Cocoa

class KiminoView: NSView {
    var clock: Clock!
    
    /// redraw
    func update() {
        self.setNeedsDisplay(self.frame)
    }
    
    /// draw clock interface
    ///
    /// - Parameter dirtyRect: rect
    override func draw(_ dirtyRect: NSRect) {
        // rectangle drawing
        NSGraphicsContext.saveGraphicsState()
        let rectanglePath = NSBezierPath(roundedRect: NSRect(x: 2, y: 0, width: 150, height: 55), xRadius: 10, yRadius: 10)
        // clock background
        NSColor(red: 0, green: 0, blue: 0, alpha: 0.916).setFill()
        rectanglePath.fill()
        NSGraphicsContext.restoreGraphicsState()
        
        // digits on the clock
        let textRect = NSRect(x: 2, y: 2, width: 150, height: 55)
        let formatter = DateFormatter()
        // 24 hrs
        formatter.dateFormat = "HH:mm"
        // specific timezone
        formatter.timeZone = TimeZone.init(identifier: self.clock.timezone)
        let textTextContent = formatter.string(from: Date())
        let textStyle = NSMutableParagraphStyle()
        textStyle.alignment = .center
        let textFontAttributes = [
            .font: NSFont(name: "digital-7", size: 63)!,
            .foregroundColor: self.clock.color,
            .paragraphStyle: textStyle,
            ] as [NSAttributedString.Key: Any]
        let textTextHeight: CGFloat = textTextContent.boundingRect(with: NSSize(width: textRect.width, height: CGFloat.infinity), options: .usesLineFragmentOrigin, attributes: textFontAttributes).height
        let textTextRect: NSRect = NSRect(x: textRect.minX, y: textRect.minY + (textRect.height - textTextHeight) / 2, width: textRect.width, height: textTextHeight)
        NSGraphicsContext.saveGraphicsState()
        textRect.clip()
        textTextContent.draw(in: textTextRect.offsetBy(dx: 0, dy: 0), withAttributes: textFontAttributes)
        NSGraphicsContext.restoreGraphicsState()
    }
}
